﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class A2ZDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(SMBPConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        public static DataTable GetRecordFromTable(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "T_Table");
                    dataTable = dataSet.Tables["T_Table"];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool InsertUpdateDataBase(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static int InsertRecordToTableReturnID(string fileds, string fieldValue, string tableName)
        {
            try
            {
                sqlCommand = new SqlCommand("usp_InsertRecordToTableReturnID", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@fileds", fileds));
                sqlCommand.Parameters.Add(new SqlParameter("@values", fieldValue));
                sqlCommand.Parameters.Add(new SqlParameter("@tablename", tableName));
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        #endregion

        public static DataTable GetA2ZOpratorList(string serviceType)
        {
            return GetRecordFromTable("select * from T_SMBP_BBPSBillerList where Status=1 and serviceType='" + serviceType + "' order by Operator");
        }
        public static bool InsertA2ZPaymentDetails(string AgentId, string ClientRefId, string MobileNo, string ProviderKey, string Amount, string spkey, string customername = "")
        {
            bool issuccess = false;
            string query = "insert into T_A2Z_PaymentHistory (AgentId,ClientRefId,MobileNo,ProviderKey,Amount) "
                + "values('" + AgentId + "','" + ClientRefId + "','" + MobileNo + "','" + ProviderKey + "','" + Amount + "')";
            issuccess = InsertUpdateDataBase(query);
            if (issuccess)
            {
                string query1 = "insert into T_SMBP_BBPSPayment (AgentId,ClientRefId,Number,CircleID,Amount,BookingType,SPKey,CustomerName) "
                   + "values('" + AgentId + "','" + ClientRefId + "','" + MobileNo + "','" + ProviderKey + "','" + Amount + "','A2Z_Suvidhaa','" + spkey + "','" + customername + "')";
                bool isnoted = InsertUpdateDataBase(query1);
            }
            return issuccess;
        }
        public static bool UpdateA2ZPaymentDetails(string ClientRefId, string Status, string Message, string TxnId, string OperatorId, string ClientId, string StatusId, string AvailableBalance, string customername = "")
        {
            bool issuccess = false;
            string query = "update T_A2Z_PaymentHistory set Status='" + Status + "',Message='" + Message + "',TxnId='" + TxnId + "',OperatorId='" + OperatorId + "',ClientId='" + ClientId + "',StatusId='" + StatusId + "',AvailableBalance='" + AvailableBalance + "' where ClientRefId='" + ClientRefId + "'";
            issuccess = InsertUpdateDataBase(query);
            if (issuccess)
            {
                string query1 = "update T_SMBP_BBPSPayment set TransactionId='" + TxnId + "',AvlBalance='" + AvailableBalance + "',OptTransactionId='" + OperatorId + "',Status='" + Status + "',Refund='1',CustomerName='" + customername + "' where ClientRefId='" + ClientRefId + "'";
                bool isnoted = InsertUpdateDataBase(query1);
            }
            return issuccess;
        }
        public static bool A2ZUpdatePaymentByStatus(string billid, string transStatus)
        {
            string innerquery = "update T_SMBP_BBPSPayment set Status='" + transStatus + "', Refund=1, RefundId='' where BBPSPaymentId=" + billid;

            if (InsertUpdateDataBase(innerquery))
            {
                return true;
            }

            return false;
        }
        public static DataTable GetAgencyDetailById(string agencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agencyId));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static DataSet GetUtilityCommision(double amount, string groupType, string spKey)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetSMBP_UtilityCommissionDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Group_Type", groupType));
                sqlCommand.Parameters.Add(new SqlParameter("@SpKey", spKey));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", amount));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "commission");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        #region [Insert Remitter Details During Registration]
        public static int InsertRemitterFirstDetail(string mobile, string firstName, string lastName, string pinCode, string localadd, string agentid)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_Dmt_InsertRemitterDetail", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RegMobile", mobile);
                sqlCommand.Parameters.AddWithValue("@FirstName", firstName);
                sqlCommand.Parameters.AddWithValue("@LastName", lastName);
                sqlCommand.Parameters.AddWithValue("@PinCode", pinCode);
                sqlCommand.Parameters.AddWithValue("@AgentId", agentid);
                sqlCommand.Parameters.AddWithValue("@CurrentAddress", localadd);

                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }
        public static bool Dmt_UpdateRemitterExistDetail(string agentId, string mobile, string rem_bal, string verify)
        {
            string query = "update T_Dmt_RemitterDetail set is_verified='" + verify + "',rem_bal='" + rem_bal + "' where RegMobile='" + mobile + "' and AgentId='" + agentId + "'";
            return InsertUpdateDataBase(query);
        }
        public static bool Dmt_UpdateRemitterAllDetail(string agentId, string mobile, string fname, string lname, string rem_bal, string verify)
        {
            string query = "update T_Dmt_RemitterDetail set is_verified='" + verify + "',rem_bal='" + rem_bal + "' where FirstName='" + fname + "' and LastName='" + lname + "' and RegMobile='" + mobile + "' and AgentId='" + agentId + "'";
            return InsertUpdateDataBase(query);
        }
        public static DataTable GetT_RemitterDetail(string agentId, string regMobile)
        {
            return GetRecordFromTable("select * from T_Dmt_RemitterDetail where AgentId='" + agentId + "' and RegMobile='" + regMobile + "' and is_verified=1");
        }
        public static bool Update_DmtBeneficiaryDetail(string agentId, string mobile, string accountNumber, string ifscCode, string bankName, string beneId, string beneName, string customer_number, int is_bank_verified, int status_id)
        {
            DataTable dtBenDetail = Get_SenderBeneficiaryDetail(agentId, mobile, accountNumber, beneId);

            string query = string.Empty;
            if (dtBenDetail != null && dtBenDetail.Rows.Count > 0)
            {
                query = "update T_Dmt_BeneficiaryDetail set bene_Name='" + beneName + "',customer_number='" + customer_number + "',status_id='" + status_id + "',is_bank_verified='" + is_bank_verified + "' where agentId='" + agentId + "' and sender_mobile='" + mobile + "' and Beneficiary_Id='" + beneId + "'";
            }
            else
            {
                query = "insert into T_Dmt_BeneficiaryDetail (agentId,sender_mobile,accountNumber,benName,ifscCode,bankName,Beneficiary_Id,bene_Name,customer_number,status_id,is_bank_verified) values('" + agentId + "','" + mobile + "','" + accountNumber + "','" + beneName + "','" + ifscCode + "','" + bankName + "','" + beneId + "','" + beneName + "','" + customer_number + "','" + status_id + "','" + is_bank_verified + "')";
            }
            return InsertUpdateDataBase(query);
        }

        public static bool Update_DmtBeneficiaryDetail(int last_InstVal, string beneId)
        {
            return InsertUpdateDataBase("update T_Dmt_BeneficiaryDetail set Beneficiary_Id='" + beneId + "' where benid=" + last_InstVal);
        }
        public static int Insert_Dmt_RowBeneficiaryDetail(string agentId, string mobile, string accountNumber, string ifscCode, string bankName, string beneName)
        {
            return InsertRecordToTableReturnID("agentId,sender_mobile,accountNumber,benName,ifscCode,bankName", "'" + agentId + "','" + mobile + "','" + accountNumber + "','" + beneName + "','" + ifscCode + "','" + bankName + "'", "T_Dmt_BeneficiaryDetail");
        }
        public static DataTable Get_SenderBeneficiaryDetail(string agentId, string regMobile, string accountNumber = "", string beneficiary_Id = "")
        {
            DataTable dtBenDetail = new DataTable();
            if (string.IsNullOrEmpty(accountNumber.Trim()) && string.IsNullOrEmpty(beneficiary_Id.Trim()))
            {
                dtBenDetail = GetRecordFromTable("select * from T_Dmt_BeneficiaryDetail where AgentId='" + agentId + "' and sender_mobile='" + regMobile + "'");
            }
            else
            {
                dtBenDetail = GetRecordFromTable("select * from T_Dmt_BeneficiaryDetail where AgentId='" + agentId + "' and sender_mobile='" + regMobile + "' and accountNumber='" + accountNumber + "' and Beneficiary_Id='" + beneficiary_Id + "'");
            }
            return dtBenDetail;
        }
        public static DataTable GetBindAllBank(string type = "")
        {
            string query = string.Empty;
            if (type == "top")
            {
                query = "select * from T_DMT_BankList where bankid in (2,4,6,3,14,8,7,272) and status=1 order by bankname";
            }
            else if (type == "topelse")
            {
                query = "select * from T_DMT_BankList where bankid not in (2,4,6,3,14,8,7,272) and status=1 order by bankname";
            }
            else
            {
                query = "select * from T_DMT_BankList where and status=1 order by bankname";
            }

            return GetRecordFromTable(query);
        }
        public static bool Dmt_DeleteBeneficiaryDetail(string agentId, string mobile, string benid)
        {
            return InsertUpdateDataBase("delete from T_Dmt_BeneficiaryDetail where agentId='" + agentId + "' and sender_mobile='" + mobile + "' and Beneficiary_Id='" + benid + "'");
        }
        public static DataTable GetFundTransferVeryficationDetail(string agentId, string benId, string mobile, ref DataTable dtSenderDel)
        {
            DataTable benDetail = new DataTable();
            dtSenderDel = GetRecordFromTable("select * from T_Dmt_RemitterDetail where AgentId='" + agentId + "' and RegMobile='" + mobile + "' and is_verified=1");
            if (!string.IsNullOrEmpty(benId))
            {
                benDetail = GetRecordFromTable("select * from T_Dmt_BeneficiaryDetail where agentId='" + agentId + "' and customer_number='" + mobile + "' and Beneficiary_Id='" + benId + "' and status_id=1");
            }
            return benDetail;
        }

        public static int Dmt_InsertTransaction(string agentId, string mobile, string trackId, string accountNumber, string beneId, string amount, string walletType, string channelid, string channel)
        {
            string fileds = "AgentId,Mobile,TrackId,accountNumber,beneId,amount,walletType,channelid,channel";
            string fieldValue = "'" + agentId + "','" + mobile + "','" + trackId + "','" + accountNumber + "','" + beneId + "','" + amount + "','" + walletType + "','" + channelid + "','" + channel + "'";
            string tableName = "T_Dmt_Transaction";
            return InsertRecordToTableReturnID(fileds, fieldValue, tableName);
        }

        public static bool Dmt_UpdateTransaction(string agentId, string mobile, string txnId, string bankRefNo, string statusId, string status, string message, string clientId, int inserted_Val)
        {
            string query = "update T_Dmt_Transaction set txnId='" + txnId + "',bankRefNo='" + bankRefNo + "',statusId='" + statusId + "',status='" + status + "',message='" + message + "',updatedDate=getdate(),issuccuess='1' where AgentId='" + agentId + "' and Mobile='" + mobile + "' and TrackId='" + clientId + "' and TransId=" + inserted_Val;
            return InsertUpdateDataBase(query);
        }
        public static bool Dmt_UpdateTransactionByTrackId(string agentId, string mobile, string txnId, string bankRefNo, string statusId, string status, string message, string clientId, string issuccuess)
        {
            string query = "update T_Dmt_Transaction set txnId='" + txnId + "',bankRefNo='" + bankRefNo + "',statusId='" + statusId + "',status='" + status + "',message='" + message + "',updatedDate=getdate(),issuccuess='" + issuccuess + "' where AgentId='" + agentId + "' and Mobile='" + mobile + "' and TrackId='" + clientId + "'";
            return InsertUpdateDataBase(query);
        }
        public static DataTable Get_TransactionDetail(string agentId, string mobile, string trackId, string txnId = "")
        {
            return GetRecordFromTable("select * from T_Dmt_Transaction where AgentId='" + agentId + "' and Mobile='" + mobile + "' and TrackId='" + trackId + "'" + (!string.IsNullOrEmpty(txnId) ? " and txnId='" + txnId + "'" : string.Empty));
        }
        public static bool Dmt_UpdateRemitterRemaingAMount(string agentId, string mobile, string remaingamt)
        {
            string query = "update T_Dmt_RemitterDetail set rem_bal='" + remaingamt + "',updatedDate=getdate() where RegMobile='" + mobile + "' and AgentId='" + agentId + "'";
            return InsertUpdateDataBase(query);
        }

        public static DataTable Get_Dmt_TranstionHistory(string agentId, string mobile, string fromdate, string todate = "", string trackid = "", string filestatus = "")
        {
            string query = "select t.updatedDate,t.txnId,t.bankRefNo,t.TrackId,t.amount,t.channel,t.statusId,t.status,t.issuccuess,t.isrefund,b.bene_Name,b.Beneficiary_Id,b.accountNumber,b.ifscCode,b.bankName from T_Dmt_Transaction t left join T_Dmt_BeneficiaryDetail b on t.beneId=b.Beneficiary_Id where t.AgentId='" + agentId + "' and t.Mobile='" + mobile + "'";
            query = query + (!string.IsNullOrEmpty(fromdate) ? " and t.updatedDate>=CONVERT(datetime,'" + GetDateFormate(fromdate) + "')" : string.Empty);
            query = query + (!string.IsNullOrEmpty(todate) ? " and t.updatedDate<=CONVERT(datetime,'" + GetDateFormate(fromdate) + " 23:59:59')" : string.Empty);
            query = query + (!string.IsNullOrEmpty(trackid) ? " and t.TrackId='" + trackid + "'" : string.Empty);
            query = query + (!string.IsNullOrEmpty(filestatus) ? "" : string.Empty);
            query = query + " and channel<>'' order by t.updatedDate desc";
            return GetRecordFromTable(query);
        }

        private static string GetDateFormate(string date)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(date))
            {
                string[] strDate = date.Split('/');

                result = strDate[2] + "-" + strDate[1] + "-" + strDate[0];
            }

            return result;
        }
        #endregion
    }
}