﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class LedgerService
    {
        public static List<string> LedgerDebitCreditUtility(double Amount, string spkey, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            return LedgerDataBase.LedgerDebitCreditUtility(Amount, spkey, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
        }
        public static DataSet GetSMBPUtilityCommission(string amount, string agentGroupType, string spkey)
        {
            return LedgerDataBase.GetSMBPUtilityCommission(amount, agentGroupType, spkey);
        }

        public static List<string> Dmt_LedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, bool ismarkup)
        {
            return LedgerDataBase.Dmt_LedgerDebitCredit(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, ismarkup);
        }
    }
}
