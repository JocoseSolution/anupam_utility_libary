﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public class A2ZServiceCredential
    {
        public string Department { get; set; }
        public string Api_Token { get; set; }
        public string UserId { get; set; }
        public string SecretKey { get; set; }
        public string PostUrl { get; set; }
        public string PostType { get; set; }
    }
    public class ProviderList
    {
        public int providerKey { get; set; }
        public string serviceType { get; set; }
        public int serviceId { get; set; }
        public string providerName { get; set; }
    }
    public class RootProvider
    {
        public int status { get; set; }
        public List<ProviderList> providerList { get; set; }
    }

    public class Record
    {
        public string rs { get; set; }
        public string desc { get; set; }
    }

    public class RootOffer
    {
        public string tel { get; set; }
        public string @operator { get; set; }
        public List<Record> records { get; set; }
        public int status { get; set; }
        public double time { get; set; }
    }
}
