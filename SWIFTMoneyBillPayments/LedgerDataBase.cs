﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class LedgerDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(SMBPConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        public static List<string> LedgerDebitCreditUtility(double Amount, string spkey, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            List<string> HS = new List<string>();

            try
            {
                string commisionType = string.Empty;
                string charges = string.Empty;
                double commissionCharge = 0; double applyCharge = 0;
                string agencyCreditLimit = string.Empty;
                //Amount = Math.Abs(Amount);

                DataTable dtAgencyDel = A2ZDataBase.GetAgencyDetailById(AgentId);
                if (dtAgencyDel != null && dtAgencyDel.Rows.Count > 0)
                {
                    agencyCreditLimit = dtAgencyDel.Rows[0]["Crd_Limit"].ToString();
                    string groupType = dtAgencyDel.Rows[0]["Agent_Type"].ToString();

                    if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(Amount))
                    {
                        DataSet dsCommisionTDS = A2ZDataBase.GetUtilityCommision(Amount, groupType, spkey);
                        if (dsCommisionTDS != null && dsCommisionTDS.Tables.Count > 0)
                        {
                            commisionType = dsCommisionTDS.Tables[0].Rows[0]["Charges_Type"].ToString();
                            charges = dsCommisionTDS.Tables[0].Rows[0]["Charges"].ToString();

                            //double reminder = Math.Ceiling(Amount / Convert.ToDouble(commamount));

                            if (commisionType.ToLower() == "fixed")
                            {
                                commissionCharge = Convert.ToDouble(charges);
                            }
                            else
                            {
                                commissionCharge = (Amount * Convert.ToDouble(charges)) / 100;
                            }

                            if (dsCommisionTDS.Tables[1] != null)
                            {
                                double tdsCharges = 0;
                                string tdsType = dsCommisionTDS.Tables[1].Rows[0]["Type"].ToString();
                                string tdsAmt = dsCommisionTDS.Tables[1].Rows[0]["TDS"].ToString();

                                if (tdsType == "fixed")
                                {
                                    tdsCharges = Convert.ToDouble(tdsAmt);
                                }
                                else
                                {
                                    tdsCharges = (commissionCharge * Convert.ToDouble(tdsAmt)) / 100;
                                }

                                applyCharge = commissionCharge - tdsCharges;
                            }
                            else
                            {
                                applyCharge = commissionCharge;
                            }
                        }

                        Amount = Amount - applyCharge;
                    }
                }

                if (Uploadtype.Trim().ToLower() == "dr")
                {
                    Debit = Debit - applyCharge;

                    if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(Debit))
                    {
                        HS = DebitCreditAFromLedger(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
                    }
                }
                else
                {
                    Credit = Credit - applyCharge;
                    HS = DebitCreditAFromLedger(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }

        private static List<string> DebitCreditAFromLedger(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            List<string> HS = new List<string>();
            sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.AddWithValue("@Amount", Math.Abs(Amount));
            sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
            sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
            sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
            sqlCommand.Parameters.AddWithValue("@PnrNo", "");
            sqlCommand.Parameters.AddWithValue("@TicketNo", "");
            sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
            sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
            sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
            sqlCommand.Parameters.AddWithValue("@ExecutiveID", AgentId);
            sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
            sqlCommand.Parameters.AddWithValue("@Debit", Debit);
            sqlCommand.Parameters.AddWithValue("@Credit", Credit);
            sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
            sqlCommand.Parameters.AddWithValue("@Remark", Remark);
            sqlCommand.Parameters.AddWithValue("@PaxId", 0);
            sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
            sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
            sqlCommand.Parameters.AddWithValue("@ID", 1);
            sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
            sqlCommand.Parameters.AddWithValue("@Type", "Con");
            sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
            sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
            sqlCommand.Parameters.AddWithValue("@BankName", "");
            sqlCommand.Parameters.AddWithValue("@BankCode", "");
            sqlCommand.Parameters.AddWithValue("@Narration", Narration);
            sqlCommand.Parameters.AddWithValue("@TransType", TransType);
            sqlCommand.Parameters.AddWithValue("@ModuleType", "");
            sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
            //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
            sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

            MyAmdOpenConnection();
            int isSuccess = sqlCommand.ExecuteNonQuery();
            double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
            string result = sqlCommand.Parameters["@result"].Value.ToString();
            MyAmdCloseConnection();

            HS.Add(result);
            HS.Add(Aval_Balance.ToString());

            return HS;
        }
        public static DataSet GetSMBPUtilityCommission(string amount, string agentGroupType, string spkey)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetSMBP_UtilityCommissionDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Group_Type", agentGroupType));
                sqlCommand.Parameters.Add(new SqlParameter("@SpKey", spkey));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", amount));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        #region [A2Z Section]
        public static DataTable GetA2ZServiceCredential(string department, string service)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetA2ZServiceCredential", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Department", department));
                sqlCommand.Parameters.Add(new SqlParameter("@Service", service));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool InsertA2ZProvider(string providerKey, string serviceType, string serviceId, string providerName)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InsertA2ZProvider", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@providerKey", providerKey));
                sqlCommand.Parameters.Add(new SqlParameter("@serviceType", serviceType));
                sqlCommand.Parameters.Add(new SqlParameter("@serviceId", serviceId));
                sqlCommand.Parameters.Add(new SqlParameter("@providerName", providerName));

                if (sqlCommand.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static List<string> Dmt_LedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, bool ismarkup = true)
        {
            List<string> result = new List<string>();
            try
            {
                string charges_Type = string.Empty;
                string charges = string.Empty;
                string agencyCreditLimit = string.Empty;
                double applyCharge = 0;
                Amount = Math.Abs(Amount);

                DataTable dtAgencyDel = A2ZDataBase.GetAgencyDetailById(AgentId);
                if (dtAgencyDel != null && dtAgencyDel.Rows.Count > 0)
                {
                    agencyCreditLimit = dtAgencyDel.Rows[0]["Crd_Limit"].ToString();
                    string groupType = dtAgencyDel.Rows[0]["Agent_Type"].ToString();

                    if (Convert.ToDouble(agencyCreditLimit) >= Convert.ToDouble(Amount))
                    {
                        if (ismarkup == true)
                        {
                            DataTable dtMarkup = GetDMT_MarkupDtail(Amount, groupType);
                            if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                            {
                                charges_Type = dtMarkup.Rows[0]["Charges_Type"].ToString();
                                charges = dtMarkup.Rows[0]["Charges"].ToString();

                                if (charges_Type.ToLower() == "fixed")
                                {
                                    applyCharge = Convert.ToDouble(charges);
                                }
                                else
                                {
                                    applyCharge = (Amount * Convert.ToDouble(charges)) / 100;
                                }
                            }

                            Amount = Amount + applyCharge;
                            if (Uploadtype.Trim().ToLower() == "dr")
                            {
                                Debit = Debit + applyCharge;
                            }
                            else
                            {
                                Credit = Credit + applyCharge;
                            }
                        }

                        result = DebitCreditAFromLedger(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static DataTable GetDMT_MarkupDtail(double amount, string groupType) //sp_GetDMTMarkupCharges
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetDMTMarkupCharges", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@MinAmount", amount));
                sqlCommand.Parameters.Add(new SqlParameter("@MaxAmount", amount));
                sqlCommand.Parameters.Add(new SqlParameter("@GroupType", groupType));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        #endregion
    }
}
