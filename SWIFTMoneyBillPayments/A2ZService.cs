﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class A2ZService
    {
        public static bool InsertProviderDetails(string agentId)
        {
            bool inserted = false;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Get_Provider");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\"}";
                    string response = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_GetProvider", agentId, Common_Post.GetTrackId(), "Utility", false);

                    if (!string.IsNullOrEmpty(response))
                    {
                        RootProvider provider = JsonConvert.DeserializeObject<RootProvider>(response);
                        if (provider.status == 1)
                        {
                            foreach (var item in provider.providerList)
                            {
                                bool isInserted = LedgerDataBase.InsertA2ZProvider(item.providerKey.ToString(), item.serviceType, item.serviceId.ToString(), item.providerName);
                                if (isInserted)
                                {
                                    inserted = true;
                                }
                                else
                                {
                                    inserted = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return inserted;
        }
        public static string GetRechargeOffer(string mobileNumber, int providerId, string providerName, string agentId, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Get_Recharge_Offer");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":" + credential.UserId + ",\"secretKey\":\"" + credential.SecretKey + "\",\"mobile_number\":\"" + mobileNumber + "\",\"provider\":" + providerId + "}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_GetRechargeOffer", agentId, Common_Post.GetTrackId(), "Utility", true);
                    //result = PostRestClient(credential.PostType, credential.PostUrl, rowRequest, "A2Z_GetRechargeOffer", agentId, Common_Post.GetTrackId(), true);                    
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                result = ex.Message;
            }
            return result;
        }
        public static string GetGetDTHOffer(string dthnumber, int providerKey, string providerName, string agentId, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Get_DTH_Offer");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\",\"number\":\"" + dthnumber + "\",\"provider\":" + providerKey + "}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_GetGetDTHOffer", agentId, Common_Post.GetTrackId(), "Utility", true);
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                result = ex.Message;
            }
            return result;
        }
        public static string RechargePayment(string providerKey, double amount, string number, string agentId, string clientId, string spkey, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Pay_Recharge");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    if (InsertA2ZPaymentDetails(agentId, clientId, number, providerKey, amount.ToString(), spkey))
                    {
                        string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":" + credential.UserId + ",\"secretKey\":\"" + credential.SecretKey + "\",\"providerKey\":\"" + providerKey + "\",\"amount\":" + amount + ",\"clientId\":\"" + clientId + "\",\"number\":\"" + number + "\"}";

                        result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_Payment", agentId, clientId, "Utility", true);

                        if (!string.IsNullOrEmpty(result))
                        {
                            dynamic rech = JObject.Parse(result);

                            string status = rech.status;
                            string message = rech.message;
                            string txnId = rech.txnId;
                            string operatorId = rech.operatorId;
                            string respoclientId = rech.clientId;
                            string statusId = rech.statusId;
                            string availableBalance = rech.availableBalance;

                            bool isUpdated = UpdateA2ZPaymentDetails(clientId, status, message, txnId, operatorId, respoclientId, statusId, availableBalance);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                ex.ToString();
            }
            return result;
        }
        public static string PostPaidRechargePayment(string providerKey, double amount, string number, string agentId, string clientId, string spkey, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("PostPay_Recharge");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    if (InsertA2ZPaymentDetails(agentId, clientId, number, providerKey, amount.ToString(), spkey))
                    {
                        string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":" + credential.UserId + ",\"secretKey\":\"" + credential.SecretKey + "\",\"provider\":\"" + providerKey + "\",\"amount\":" + amount + ",\"clientId\":\"" + clientId + "\",\"number\":\"" + number + "\"}";

                        result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_Payment", agentId, clientId, "Utility", true);

                        if (!string.IsNullOrEmpty(result))
                        {
                            dynamic rech = JObject.Parse(result);

                            string status = rech.status;
                            string message = rech.message;
                            string txnId = rech.txnId;
                            string operatorId = rech.operatorId;
                            string respoclientId = rech.clientId;
                            string statusId = rech.statusId;
                            string availableBalance = rech.availableBalance;

                            bool isUpdated = UpdateA2ZPaymentDetails(clientId, status, message, txnId, operatorId, respoclientId, statusId, availableBalance);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                ex.ToString();
            }
            return result;
        }
        public static string GetAirtelPostPaidBill(string mobile, string providerKey, string agentId, ref string msg)
        {
            string result = string.Empty;

            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Mobile_FetchBill");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\",\"number\":\"" + mobile + "\",\"provider\":" + providerKey + "}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_GetAirtelPostPaidBill", agentId, Common_Post.GetTrackId(), "Utility", true);
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                result = ex.Message;
            }
            return result;
        }
        public static string A2ZPaymentEnquiry(string agentId, string clientRefId, string transactionId, string transStatus)
        {
            string result = string.Empty;

            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Check_Payment_Status");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\",\"clientId\":\"" + clientRefId + "\"}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_CheckPaymentStatus", agentId, Common_Post.GetTrackId(), "Utility", true);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static DataTable GetAgencyDetailById(string agencyId)
        {
            return A2ZDataBase.GetAgencyDetailById(agencyId);
        }
        public static bool A2ZUpdatePaymentByStatus(string billid, string transStatus)
        {
            return A2ZDataBase.A2ZUpdatePaymentByStatus(billid, transStatus);
        }
        public static string CheckBalance(string agentId, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Check_Balance");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\"}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_CheckBalance", agentId, Common_Post.GetTrackId(), "Utility", true);
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                ex.ToString();
            }
            return result;
        }
        public static string CheckStatus(string clientId, string agentId, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Check_Status");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\",\"clientId\":\"" + clientId + "\"}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_CheckStatus", agentId, Common_Post.GetTrackId(), "Utility", true);
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                ex.ToString();
            }
            return result;
        }
        public static string GetIp(string agentId)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Get_Ip");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\"}";
                    result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "A2Z_GetIp", agentId, Common_Post.GetTrackId(), "Utility", true);
                    if (!string.IsNullOrEmpty(result))
                    {
                        JObject jo_response = JObject.Parse(result);
                        var status = jo_response["status"];
                        var message = jo_response["message"];
                        if (status.ToString() == "1")
                        {
                            result = result.Replace("Your ip is ", "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static string GetA2ZOpratorList(string serviceType, string selectname)
        {
            string result = string.Empty;

            DataTable dtOprator = A2ZDataBase.GetA2ZOpratorList(serviceType);

            if (dtOprator != null && dtOprator.Rows.Count > 0)
            {
                List<string> iscontain = new List<string>();

                result = "<option value='0'>--Select " + selectname + "--</option>";
                for (int i = 0; i < dtOprator.Rows.Count; i++)
                {
                    if (!iscontain.Contains(dtOprator.Rows[i]["Operator"].ToString()))
                    {
                        result = result + "<option value='" + dtOprator.Rows[i]["BillerId"].ToString() + "' data-servicetype='" + dtOprator.Rows[i]["ServiceType"].ToString() + "' data-serviceid='" + dtOprator.Rows[i]["FetchId"].ToString() + "' data-spkey='" + dtOprator.Rows[i]["SpKey"].ToString() + "'>" + dtOprator.Rows[i]["Operator"].ToString() + "</option>";

                        iscontain.Add(dtOprator.Rows[i]["Operator"].ToString());
                    }
                }
            }

            return result;
        }
        public static bool InsertA2ZPaymentDetails(string AgentId, string ClientRefId, string MobileNo, string ProviderKey, string Amount, string spkey, string customername = "")
        {
            return A2ZDataBase.InsertA2ZPaymentDetails(AgentId, ClientRefId, MobileNo, ProviderKey, Amount, spkey, customername);
        }
        public static bool UpdateA2ZPaymentDetails(string ClientRefId, string Status, string Message, string TxnId, string OperatorId, string ClientId, string StatusId, string AvailableBalance, string customername = "")
        {
            return A2ZDataBase.UpdateA2ZPaymentDetails(ClientRefId, Status, Message, TxnId, OperatorId, ClientId, StatusId, AvailableBalance, customername);
        }
        public static string PostRestClient(string postType, string Url, string ReqJson, string actionType, string agentId, string clientRefId, bool issave = false)
        {
            string result = string.Empty;
            try
            {
                if (issave)
                {
                    SMBPDataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, clientRefId, "Utility");
                }

                var client = new RestClient("https://partners.a2zsuvidhaa.com/api/v3/recharge/get-special-offer");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Cookie", "a2zsuvidhaa_session=eyJpdiI6InRqU2cyXC9adWFERGo1UGRER1VMaG5nPT0iLCJ2YWx1ZSI6ImQ5bDFhdHV1RElucEpxWUpOdEpzY0xTMGVDaUs2RVwvK29raHJURXJJaUdYZkVnZDdlZElVYk12SFRCbW45RWIrIiwibWFjIjoiNTYzODFlOWQ5NjYyNGFjNDZhN2RlMWE4YTlkYzI0NTBlZjc3NzVkNWIyMWFiMTkyMWJmNTVmNzU0YThhYWIzNyJ9");
                request.AddParameter("application/json", "{\r\n    \"api_token\": \"WjJE8vMfey4eS8KJoy9U49AvIEqsCdld3voIWLf50F7jeg610pqW4i7YTRWj\",\r\n    \"userId\": 12713,\r\n    \"secretKey\": \"12713lsp1063t5BSi23mKAGiFYLwYBNcctTYkmwmLhB24H0goiiS5S6Xx542OBh7p\",\r\n    \"mobile_number\": \"9555266308\",\r\n    \"provider\": 1\r\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                result = response.Content;
            }
            catch (Exception ex)
            {
                if (issave)
                {
                    SMBPDataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + result + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, clientRefId, "Utility");
                }
            }
            finally
            {
                result = string.Empty;
            }

            if (issave)
            {
                SMBPDataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, result, actionType, agentId, clientRefId, "Utility");
            }

            return result;
        }

        #region [All Bills Fetch]
        public static List<string> FetchAllBillDetails(string number, string provider, string agentId, string actionType)
        {
            List<string> result = new List<string>();

            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Mobile_FetchBill");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\",\"number\":\"" + number + "\",\"provider\":" + provider + "}";
                    string response = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, actionType, agentId, Common_Post.GetTrackId(), "Utility", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        result.Add("fetch");
                        result.Add(response);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("fail");
                result.Add(ex.Message);
            }
            return result;
        }

        public static string PayFetchedBillPayment(string providerKey, double amount, string number, string agentId, string clientId, string spkey, string customername, string actiontype, ref string msg)
        {
            string result = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("PostPay_Recharge");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    if (InsertA2ZPaymentDetails(agentId, clientId, number, providerKey, amount.ToString(), spkey, customername))
                    {
                        string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":" + credential.UserId + ",\"secretKey\":\"" + credential.SecretKey + "\",\"provider\":\"" + providerKey + "\",\"amount\":" + amount + ",\"clientId\":\"" + clientId + "\",\"number\":\"" + number + "\"}";

                        result = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, (actiontype + "_Payment"), agentId, clientId, "Utility", true);

                        if (!string.IsNullOrEmpty(result))
                        {
                            dynamic rech = JObject.Parse(result);

                            string status = rech.status;
                            string message = rech.message;
                            string txnId = rech.txnId;
                            string txnTime = rech.txnTime;
                            string operator_ref = rech.operator_ref;
                            string billerName = rech.billerName;
                            string providerName = rech.providerName;
                            string respoamount = rech.amount;
                            string statusId = rech.statusId;

                            bool isUpdated = UpdateA2ZPaymentDetails(clientId, status, message, txnId, operator_ref, "", statusId, "", customername);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                ex.ToString();
            }
            return result;
        }
        #endregion
    }
}
