﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class SMBPConfig
    {
        public static string MyAmdDBConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString; }
        }
    }
}
